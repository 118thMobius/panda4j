/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.async;

import org.panda4j.core.*;
import org.panda4j.core.util.PandaLoader;

/**
 *
 * @author mobius
 */
public interface AsyncPanda {
    
    public void addListener(PandaListener listener);
    
    public void removeListener(PandaListener listener);

    public void search(Query query, int page);

    public void loadGallery(GalleryId id);
    
    public void loadGallery(String url);

    @Deprecated
    public void loadGalleryPage(GalleryId id, int page);

    public void loadGalleryPage(Gallery gallery, int page);

    public void loadMedia(MediaId id);

    public void reLoadMedia(Media media);
    
    public PandaLoader getPandaLoader();
}
