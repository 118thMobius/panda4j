/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.async;

import org.panda4j.core.AuthenticatedConfiguration;
import org.panda4j.core.AuthenticatedPanda;
import org.panda4j.core.Configuration;
import org.panda4j.core.PandaFactory;
import org.panda4j.core.PandaFactoryException;

/**
 *
 * @author mobius
 */
public class AsyncPandaFactory {

    private Configuration config;

    public AsyncPandaFactory setConfig(Configuration config) {
        this.config = config;
        return this;
    }

    public AsyncPanda build() {
        try {
            if (config != null) {
                PandaFactory pandaFactory = new PandaFactory();
                pandaFactory.setConfig(config);
                if (config instanceof AuthenticatedConfiguration) {
                    return new AuthenticatedAsyncPandaImpl((AuthenticatedPanda) pandaFactory.build(), config);
                } else {
                    return new AsyncPandaImpl(pandaFactory.build(), config);
                }
            }
        } catch (PandaFactoryException ex) {
            throw new IllegalStateException(ex);
        }
        throw new IllegalStateException("Configがセットされていません。");
    }
}
