/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.async;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.panda4j.core.Auth;
import org.panda4j.core.AuthException;
import org.panda4j.core.Authenticator;

/**
 *
 * @author mobius
 */
public class AsyncAuthenticator {

    private final ArrayList<AuthListener> listeners = new ArrayList<>();
    private final Authenticator authenticator = new Authenticator();
    private final Executor executor = Executors.newCachedThreadPool(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        }
    });

    public void authentication(final String userName, final String passWord) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Auth auth = authenticator.authentication(userName, passWord);
                    for (AuthListener listener : listeners) {
                        listener.authenticated(auth);
                    }
                } catch (AuthException ex) {
                    for (AuthListener listener : listeners) {
                        listener.onException(ex);
                    }
                }
            }
        });
    }

    public void addListener(AuthListener listener) {
        listeners.add(listener);
    }
}
