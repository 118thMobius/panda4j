/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.async;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.panda4j.core.Configuration;
import org.panda4j.core.Gallery;
import org.panda4j.core.GalleryId;
import org.panda4j.core.GalleryPage;
import org.panda4j.core.Media;
import org.panda4j.core.MediaId;
import org.panda4j.core.Panda;
import org.panda4j.core.PandaException;
import org.panda4j.core.Query;
import org.panda4j.core.SearchResult;
import org.panda4j.core.util.PandaLoader;

/**
 *
 * @author mobius
 */
public class AsyncPandaImpl implements AsyncPanda {

    protected final ArrayList<PandaListener> listeners = new ArrayList<>();
    protected final Executor executor = getExecutor();
    protected final Panda panda;
    protected final Configuration config;

    AsyncPandaImpl(Panda panda, Configuration config) {
        this.panda = panda;
        this.config = config;
    }

    @Override
    public void addListener(PandaListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(PandaListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void search(final Query query, int page) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    SearchResult sr = panda.search(query, 0);
                    for (PandaListener listener : listeners) {
                        listener.searched(sr);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public void loadGallery(final GalleryId id) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Gallery gallery = panda.loadGallery(id);
                    for (PandaListener listener : listeners) {
                        listener.loadedGallery(gallery);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });

    }

    @Override
    public void loadGallery(final String url) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Gallery gallery = panda.loadGallery(url);
                    for (PandaListener listener : listeners) {
                        listener.loadedGallery(gallery);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public void loadGalleryPage(final GalleryId id, final int page) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    GalleryPage p = panda.loadGalleryPage(id, page);
                    for (PandaListener listener : listeners) {
                        listener.loadedGalleryPage(p);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public void loadGalleryPage(final Gallery gallery, final int page) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    GalleryPage p = panda.loadGalleryPage(gallery, page);
                    for (PandaListener listener : listeners) {
                        listener.loadedGalleryPage(p);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });

    }

    @Override
    public void loadMedia(final MediaId id) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Media media = panda.loadMedia(id);
                    for (PandaListener listener : listeners) {
                        listener.loadedMedia(media);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public void reLoadMedia(final Media media) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Media m = panda.reLoadMedia(media);
                    for (PandaListener listener : listeners) {
                        listener.loadedMedia(m);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public PandaLoader getPandaLoader() {
        return panda.getPandaLoader();
    }

    protected void onException(AsyncPandaException ex) {
        for (PandaListener listener : listeners) {
            listener.onException(ex);
        }
    }

    protected Executor getExecutor() {
        return Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });
    }
}
