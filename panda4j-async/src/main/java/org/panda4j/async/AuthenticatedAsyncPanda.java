/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.async;

import org.panda4j.core.GalleryId;

/**
 *
 * @author mobius
 */
public interface AuthenticatedAsyncPanda extends AsyncPanda{

    public void addToFavorite(GalleryId id, int file, String note);

    public void removeFromFavorite(GalleryId id);

    public void loadFavoriteFoldersTitle();
    
}
