/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.async;

import org.panda4j.core.AuthenticatedPanda;
import org.panda4j.core.Configuration;
import org.panda4j.core.GalleryId;
import org.panda4j.core.Panda;
import org.panda4j.core.PandaException;

/**
 *
 * @author mobius
 */
public class AuthenticatedAsyncPandaImpl extends AsyncPandaImpl implements AuthenticatedAsyncPanda{

    public AuthenticatedAsyncPandaImpl(AuthenticatedPanda panda, Configuration config) {
        super(panda, config);
    }

    @Override
    public void addToFavorite(final GalleryId id, final int file, final String note) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ((AuthenticatedPanda) panda).addToFavorite(id, file, note);
                    for (PandaListener listener : listeners) {
                        listener.addedToFavorite(id);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public void removeFromFavorite(final GalleryId id) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ((AuthenticatedPanda) panda).removeFromFavorite(id);
                    for (PandaListener listener : listeners) {
                        listener.removedFromFavorite(id);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }

    @Override
    public void loadFavoriteFoldersTitle() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String[] titles = ((AuthenticatedPanda) panda).loadFavoriteFoldersTitle();
                    for (PandaListener listener : listeners) {
                        listener.loadedFavoriteFoldersTitle(titles);
                    }
                } catch (PandaException ex) {
                    onException(new AsyncPandaException(ex));
                }
            }
        });
    }
    
}
