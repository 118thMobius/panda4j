/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.async;

import org.panda4j.core.Gallery;
import org.panda4j.core.GalleryId;
import org.panda4j.core.GalleryPage;
import org.panda4j.core.Media;
import org.panda4j.core.SearchResult;

/**
 *
 * @author mobius
 */
public class PandaAdapter implements PandaListener{

    @Override
    public void searched(SearchResult result) {
    }

    @Override
    public void addedToFavorite(GalleryId favorited) {
    }

    @Override
    public void removedFromFavorite(GalleryId unfavorited) {
    }

    @Override
    public void loadedGallery(Gallery gallery) {
    }

    @Override
    public void loadedGalleryPage(GalleryPage page) {
    }

    @Override
    public void loadedFavoriteFoldersTitle(String[] titles) {
    }

    @Override
    public void loadedMedia(Media media) {
    }

    @Override
    public void reLoadedMedia(Media media) {
    }

    @Override
    public void onException(Exception e) {
    }
    
}
