/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.panda4j.core.Tag;
import org.panda4j.core.TagType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author mobius
 */
class TagToolImpl implements TagTool {

    protected int userLevel = 5;
    protected final HashMap<TagType, HashMap<String, String>> tagMap = new HashMap<>();
    protected final APIConnectionBuilder connectionBuilder;

    protected TagToolImpl(Authentication authentication) {
        for (TagType t : TagType.values()) {
            tagMap.put(t, new HashMap<String, String>());
        }
        connectionBuilder = new APIConnectionBuilder(authentication);
    }

    static protected String getElementContent(Element e, String TagName) {
        NodeList nl = e.getElementsByTagName(TagName);
        Node n = nl.item(0);
        Node content = n.getFirstChild();
        return content.getNodeValue();
    }

    protected int verify() {
        try {
            APIConnection connection = connectionBuilder.build("https://api.panda4j.org/user/verify.php/");
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
            Element root = document.getDocumentElement();
            Element user = (Element) root.getElementsByTagName("user").item(0);
            userLevel = Integer.valueOf(getElementContent(user, "level"));
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | ParserConfigurationException | SAXException | AuthException ex) {
            Logger.getLogger(TagToolImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userLevel;
    }

    protected String formatTagString(Tag tag) {
        return tag.getTagString().replace(" ", "%20").toLowerCase();
    }

    @Override
    public String translate(Tag tag) throws TranslateException, TagToolException {
        if (tagMap.get(tag.getTagType()).containsKey(formatTagString(tag))) {
            return tagMap.get(tag.getTagType()).get(formatTagString(tag));
        } else {
            try {
                APIConnection connection = connectionBuilder.build("https://api.panda4j.org/dictionary/translate.php/?tagtype=" + tag.getTagType().name().toLowerCase() + "&keyword=" + formatTagString(tag));
                Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
                Element root = document.getDocumentElement();
                NodeList words = root.getElementsByTagName("word");
                Element e = (Element) words.item(0);
                switch (getElementContent(e, "status")) {
                    case "OK":
                        tagMap.get(tag.getTagType()).put(formatTagString(tag), getElementContent(e, "value"));
                        return getElementContent(e, "value");
                    case "ADD":
                        throw new TranslateException("未翻訳のタグです。翻訳キューに追加されました。").setTag(tag).setStatus(TranslateException.Status.ADD);
                    case "ADDED":
                        throw new TranslateException("翻訳キューに追加されていますが、セントラルリポジトリに適用されていないタグです。").setTag(tag).setStatus(TranslateException.Status.ADDED);
                    case "UNKNOWN":
                    default:
                        throw new TranslateException("未翻訳のタグです。").setTag(tag).setStatus(TranslateException.Status.UNKNOWN);
                }
            } catch (IOException | NoSuchAlgorithmException | KeyManagementException | SAXException | ParserConfigurationException | AuthException ex) {
                throw new TagToolException("翻訳中に例外が発生しました。", ex);
            }
        }
    }

    @Override
    public HashMap<Tag, String> translate(ArrayList<Tag> list) {
        return null;
    }

    @Override
    public WikiEntry getWikiEntry(Tag tag) throws TagToolException {
        try {
            Connection.Response response = Jsoup.connect("https://ehwiki.org/wiki/" + tag.getTagString().replace(" ", "_")).userAgent("Mozilla").execute();
            if (response.statusCode() == HttpURLConnection.HTTP_OK) {
                org.jsoup.nodes.Document document = response.parse();
                ArrayList<TagType> slaves = new ArrayList<>();
                String japanese = null;
                for (org.jsoup.nodes.Element parent : document.getElementById("mw-content-text").getElementsByTag("li")) {
                    if (parent.getElementsByTag("b").size() == 1) {
                        String index = parent.getElementsByTag("b").text();
                        String content = parent.text().replace(index + ": ", "").trim();
                        switch (index) {
                            case "Slave Tags":
                                for (TagType tagType : TagType.values()) {
                                    if (content.toLowerCase().contains(tagType.name().toLowerCase())) {
                                        slaves.add(tagType);
                                    }
                                }
                                break;
                            case "Japanese":
                                japanese = content;
                                break;
                            default:
                                //System.out.println(index + ":" + content);
                        }
                    }
                }
                if (japanese != null) {
                    for (TagType tagType : slaves) {
                        tagMap.get(tagType).put(tag.getTagString(), japanese);
                    }
                    return new WikiEntry(tag.getTagString(), japanese, slaves);
                }
            }
            throw new TagToolException("ページが存在しません。");
        } catch (IOException ex) {
            throw new TagToolException(ex);
        }
    }

    @Override
    public void fetch() throws TagToolException {
        try {
            for (TagType t : TagType.values()) {
                APIConnection connection = connectionBuilder.build("https://api.panda4j.org/dictionary/dictionary.php/?tagtype=" + t.name().toLowerCase());
                Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
                Element root = document.getDocumentElement();
                NodeList words = root.getElementsByTagName("word");
                for (int i = 0; i < words.getLength(); i++) {
                    Element e = (Element) words.item(i);
                    tagMap.get(t).put(e.getAttribute("keyword"), getElementContent(e, "value"));
                }
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | ParserConfigurationException | SAXException | AuthException ex) {
            throw new TagToolException("翻訳中に例外が発生しました。", ex);
        }
    }

    @Override
    public void request(Tag tag, String value) throws TagToolException {
        try {
                APIConnection connection = connectionBuilder.build("https://api.panda4j.org/dictionary/request.php/?tagtype=" + tag.getTagType().name().toLowerCase() + "&keyword=" + formatTagString(tag) + "&value=" + value.replace(" ", "%20"));
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
            Element root = document.getDocumentElement();
            NodeList words = root.getElementsByTagName("word");
            Element e = (Element) words.item(0);
            switch (getElementContent(e, "status")) {
                case "OK":
                    tagMap.get(tag.getTagType()).put(formatTagString(tag), getElementContent(e, "value"));
                    break;
                case "NG":
                    throw new TagToolException("例外が発生しました。");
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | ParserConfigurationException | SAXException | AuthException ex) {
            throw new TagToolException("例外が発生しました。", ex);
        }
    }

    @Override
    public HashMap<Tag, String> getRequestList(TagType tagType) throws TagToolException {
        HashMap<Tag, String> requestList = new HashMap<>();
        try {
            APIConnection connection = connectionBuilder.build("https://api.panda4j.org/dictionary/request_list.php/?tagtype=" + tagType.name().toLowerCase());
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
            Element root = document.getDocumentElement();
            NodeList words = root.getElementsByTagName("word");
            for (int i = 0; i < words.getLength(); i++) {
                Element e = (Element) words.item(i);
                requestList.put(new Tag(e.getAttribute("keyword"), tagType), getElementContent(e, "value"));
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | ParserConfigurationException | SAXException | AuthException ex) {
            throw new TagToolException("例外が発生しました。", ex);
        }
        return requestList;
    }

    @Override
    public void update(Tag tag, String value) throws TagToolException {
        try {
            APIConnection connection = connectionBuilder.build("https://api.panda4j.org/dictionary/update.php/?tagtype=" + tag.getTagType().name().toLowerCase() + "&keyword=" + formatTagString(tag) + "&value=" + value.replace(" ", "%20"));
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
            Element root = document.getDocumentElement();
            NodeList words = root.getElementsByTagName("word");
            Element e = (Element) words.item(0);
            switch (getElementContent(e, "status")) {
                case "OK":
                    tagMap.get(tag.getTagType()).put(formatTagString(tag), getElementContent(e, "value"));
                    break;
                case "NG":
                    throw new TagToolException("例外が発生しました。");
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | ParserConfigurationException | SAXException | AuthException ex) {
            throw new TagToolException("例外が発生しました。", ex);
        }
    }

    @Override
    public ArrayList<Tag> getUntranslatedList(TagType tagType) throws TagToolException {
        ArrayList<Tag> untranslatedList = new ArrayList<>();
        try {
            APIConnection connection = connectionBuilder.build("https://api.panda4j.org/dictionary/untranslated_list.php/?tagtype=" + tagType.name().toLowerCase());
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(connection.getInputStream());
            Element root = document.getDocumentElement();
            NodeList words = root.getElementsByTagName("word");
            for (int i = 0; i < words.getLength(); i++) {
                Element e = (Element) words.item(i);
                untranslatedList.add(new Tag(e.getAttribute("keyword"), tagType));
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException | ParserConfigurationException | SAXException | AuthException ex) {
            throw new TagToolException("例外が発生しました。", ex);
        }
        return untranslatedList;
    }
}
