/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool.async;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.panda4j.core.Tag;
import org.panda4j.core.TagType;
import org.panda4j.tagtool.Authentication;
import org.panda4j.tagtool.TagTool;
import org.panda4j.tagtool.TagToolException;
import org.panda4j.tagtool.TagToolBuilder;
import org.panda4j.tagtool.TranslateException;

/**
 *
 * @author mobius
 */
class AsyncTagToolImpl implements AsyncTagTool {

    private final TagTool tagTool;
    private List<TagToolListener> listeners = Collections.synchronizedList(new ArrayList<TagToolListener>());
    private final Executor executor = Executors.newCachedThreadPool(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
        }
    });

    AsyncTagToolImpl(Authentication auth) {
        tagTool = new TagToolBuilder().setAuth(auth).build();
    }

    @Override
    public void addListener(TagToolListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(TagToolListener listener) {
        listeners.remove(listener);
    }
    
    @Override
    public void translate(final Tag tag) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String value = tagTool.translate(tag);
                    for (TagToolListener listener : listeners) {
                        listener.translated(tag, value);
                    }
                } catch (TranslateException ex) {
                    onTranslateException(ex);
                } catch (TagToolException ex) {
                    onException(ex);
                }
            }
        });
    }

    @Override
    public void translate(ArrayList<Tag> list) {
        executor.execute(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void getRecomend(final Tag tag) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String japanese = tagTool.getWikiEntry(tag).getJapanese();
                    for (TagToolListener listener : listeners) {
                        listener.gotRecomended(tag, japanese);
                    }
                } catch (TagToolException ex) {
                    Logger.getLogger(AsyncTagToolImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    @Override
    public void request(final Tag tag, final String value) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    tagTool.request(tag, value);
                    for (TagToolListener listener : listeners) {
                        listener.requested(tag,value);
                    }
                } catch (TagToolException ex) {
                    onException(ex);
                }
            }
        });

    }

    @Override
    public void getRequestList(final TagType tagType) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    HashMap<Tag,String> list = tagTool.getRequestList(tagType);
                    for (TagToolListener listener : listeners) {
                        listener.gotRequestList(list);
                    }
                } catch (TagToolException ex) {
                    onException(ex);
                }
            }
        });
    }

    @Override
    public void update(final Tag tag, final String value) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    tagTool.update(tag, value);
                    for (TagToolListener listener : listeners) {
                        listener.updated(tag,value);
                    }
                } catch (TagToolException ex) {
                    onException(ex);
                }
            }
        });
    }

    @Override
    public void getUntranslatedList(final TagType tagType) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<Tag> list = tagTool.getUntranslatedList(tagType);
                    for (TagToolListener listener : listeners) {
                        listener.gotUntranslatedList(list);
                    }
                } catch (TagToolException ex) {
                    onException(ex);
                }
            }
        });
    }

    @Override
    public void fetch() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    tagTool.fetch();
                    for (TagToolListener listener : listeners) {
                        listener.fetched();
                    }
                } catch (TagToolException ex) {
                    onException(ex);
                }
            }
        });
    }

    private void onTranslateException(TranslateException ex) {
        for (TagToolListener listener : listeners) {
            listener.onTranslateException(ex);
        }
    }

    private void onException(TagToolException ex) {
        for (TagToolListener listener : listeners) {
            listener.onException(ex);
        }
    }

}
