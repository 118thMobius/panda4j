/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool.async;

import java.util.ArrayList;
import org.panda4j.core.Tag;
import org.panda4j.core.TagType;

/**
 *
 * @author mobius
 */
public interface AsyncTagTool {
    
    public void addListener(TagToolListener listener);
    
    public void removeListener(TagToolListener listener);

    public void translate(Tag tag);

    public void translate(ArrayList<Tag> list);
    
    public void getRecomend(Tag tag);

    public void request(Tag tag, String value);

    public void getRequestList(TagType tagType);

    public void update(Tag tag, String value);

    public void getUntranslatedList(TagType tagType);

    public void fetch();
    
}
