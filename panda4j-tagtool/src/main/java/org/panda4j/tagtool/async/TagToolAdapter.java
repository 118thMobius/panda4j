/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool.async;

import java.util.ArrayList;
import java.util.HashMap;
import org.panda4j.core.Tag;
import org.panda4j.tagtool.TagToolException;
import org.panda4j.tagtool.TranslateException;

/**
 *
 * @author mobius
 */
public class TagToolAdapter implements TagToolListener {

    @Override
    public void translated(Tag tag, String value) {
    }

    @Override
    public void translated(HashMap<Tag, String> dict) {
    }

    @Override
    public void gotRecomended(Tag tag, String japanese) {
    }

    @Override
    public void requested(Tag tag, String value) {
    }

    @Override
    public void gotRequestList(HashMap<Tag, String> lsit) {
    }

    @Override
    public void updated(Tag tag, String value) {
    }

    @Override
    public void gotUntranslatedList(ArrayList<Tag> list) {
    }

    @Override
    public void fetched() {
    }

    @Override
    public void onTranslateException(TranslateException ex) {
    }

    @Override
    public void onException(TagToolException ex) {
    }

}
