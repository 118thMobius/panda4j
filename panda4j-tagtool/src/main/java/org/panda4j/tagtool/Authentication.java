/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool;

/**
 * Panda4J.org上のAPIを利用するために必要な認証情報を扱うクラスです。
 *
 * @author mobius
 */
public class Authentication {
    private final String user;
    private final String password;

    public Authentication(String user, String password) {
        this.user = user;
        this.password = password;
    }
    
    public static Authentication getDefaultAuthentication(){
        return new Authentication("user","password");
    }

    protected String getUser() {
        return user;
    }

    protected String getPassword() {
        return password;
    }
}
