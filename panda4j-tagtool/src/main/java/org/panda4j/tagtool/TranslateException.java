/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool;

import org.panda4j.core.Tag;

/**
 * 翻訳時に発生した例外を扱うクラスです。
 *
 * @author mobius
 */
public class TranslateException extends Exception{
    private Status status = Status.UNKNOWN;
    private Tag tag;
    
    public enum Status{
        OK,
        ADD,
        ADDED,
        UNKNOWN
    }
    
    protected TranslateException setStatus(Status status){
        this.status = status;
        return this;
    }
    
    protected TranslateException setTag(Tag tag){
        this.tag = tag;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public Tag getTag() {
        return tag;
    }

    public TranslateException() {
    }

    public TranslateException(String message) {
        super(message);
    }

    public TranslateException(String message, Throwable cause) {
        super(message, cause);
    }

    public TranslateException(Throwable cause) {
        super(cause);
    }

    public TranslateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
