/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author mobius
 */
class APIConnection {
    private final HttpsURLConnection connection;

    APIConnection(HttpsURLConnection connection) {
        this.connection = connection;
    }

    void setRequestMethod(String method) throws ProtocolException {
        connection.setRequestMethod(method);
    }

    InputStream getInputStream() throws IOException, AuthException {
        connection.connect();
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            return connection.getInputStream();
        }else{
            if (connection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                throw new AuthException();
            }else{
                throw new IllegalStateException();
            }
        }
    }
}
