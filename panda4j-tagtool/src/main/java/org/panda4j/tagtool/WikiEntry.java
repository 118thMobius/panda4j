/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool;

import java.util.ArrayList;
import org.panda4j.core.TagType;

/**
 * Wiki上でタグに与えられた情報を持つクラスです。
 *
 * @author mobius
 */
public class WikiEntry {

    private final String tagString;
    private final String japanese;
    private final ArrayList<TagType> slaves;

    public WikiEntry(String tagString, String japanese, ArrayList<TagType> slaves) {
        this.tagString = tagString;
        this.japanese = japanese;
        this.slaves = slaves;
    }

    public String getTagString() {
        return tagString;
    }

    public String getJapanese() {
        return japanese;
    }

    public ArrayList<TagType> getSlaves() {
        return slaves;
    }

}
