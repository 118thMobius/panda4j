/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.tagtool;

import java.util.ArrayList;
import java.util.HashMap;
import org.panda4j.core.Tag;
import org.panda4j.core.TagType;

/**
 * TagToolはPanda4Jの提供する{@link org.panda4j.core.Tag}を日本語に翻訳するためのAPIを提供します。
 *
 * @author mobius
 */
public interface TagTool {

    public String translate(Tag tag) throws TranslateException,TagToolException;

    public HashMap<Tag, String> translate(ArrayList<Tag> list) throws TranslateException,TagToolException;

    public void update(Tag tag, String value) throws TagToolException;

    public void request(Tag tag, String value) throws TagToolException;

    public HashMap<Tag, String> getRequestList(TagType tagType) throws TagToolException;

    public ArrayList<Tag> getUntranslatedList(TagType tagType) throws TagToolException;
    
    public WikiEntry getWikiEntry(Tag tag) throws TagToolException;

    public void fetch() throws TagToolException;
}
