/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * 
 * 
 *
 * @author mobius
 */
public class ConnectableQueryBuilder implements QueryBuilder{
    private final ArrayList<Criterion> criteria = new ArrayList<>();
    private int rate = 0;
    private boolean showExpanged = false;
    private Genres genres = new GenresBuilder().enableAll().build();

    public ConnectableQueryBuilder() {
    }
    
    @Override
    public ConnectableQuery build() {
        return new ConnectableQuery(genres, criteria,rate,showExpanged);
    }
    
    public ConnectableQueryBuilder addCriterion(Criterion c){
        this.criteria.add(c);
        return this;
    }
    
    public ConnectableQueryBuilder addCriteria(Collection<? extends Criterion> criteria){
        this.criteria.addAll(criteria);
        return this;
    }
    
    public ConnectableQueryBuilder addConnectableQuery(ConnectableQuery c){
        this.criteria.addAll(c.getCriteria());
        return this;
    }
    
    public ConnectableQueryBuilder setMinimunRate(int rate){
        this.rate = rate;
        return this;
    }

    public ConnectableQueryBuilder setEnableShowExpanged(boolean showExpanged) {
        this.showExpanged = showExpanged;
        return this;
    }
    
    public ConnectableQueryBuilder setGenres(Genres genres){
        this.genres = genres;
        return this;
    }
            
    public static abstract class Criterion implements Serializable{
        abstract protected String getSearchClause();
        
        protected String formatKeyword(String keyword){
            keyword = keyword.trim()+"$";
            return keyword.contains(" ") ? "\""+keyword+"\"" : keyword;
        }
    }
    
    public static class KeyWordCriterion extends Criterion{
        private final String keyword;

        public KeyWordCriterion(String keyword) {
            this.keyword = keyword;
        }

        public String getKeyword() {
            return keyword;
        }
        
        @Override
        public String toString() {
            return getSearchClause();
        }

        @Override
        public String getSearchClause() {
            return formatKeyword(keyword);
        }
    }
    
    public static class TagCriterion extends Criterion{
        private final Tag tag;

        public TagCriterion(Tag tag) {
            this.tag = tag;
        }

        public Tag getTag() {
            return tag;
        }

        @Override
        public String toString() {
            return tag.getTagType().name().toLowerCase()+ ":" + (tag.getTagString().contains(" ") ? ("\"" + tag.getTagString() + "\"") : tag.getTagString());
        }

        @Override
        public String getSearchClause() {
            if (tag.getTagType() == TagType.MISC) {
                return formatKeyword(tag.getTagString());
            }
            return tag.getTagType().name().toLowerCase()+":"+formatKeyword(tag.getTagString());
        }
    }
}
