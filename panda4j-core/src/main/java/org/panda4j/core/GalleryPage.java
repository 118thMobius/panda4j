/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.util.Collection;

/**
 *
 * @author mobius
 */
public class GalleryPage extends MediaIds{
    private final GalleryId galleryId;
    private final int page;

    public GalleryPage(GalleryId galleryId, int page) {
        this.galleryId = galleryId;
        this.page = page;
    }

    public GalleryPage(GalleryId galleryId, int page, int initialCapacity) {
        super(initialCapacity);
        this.galleryId = galleryId;
        this.page = page;
    }

    public GalleryPage(GalleryId galleryId, int page, Collection<? extends MediaId> c) {
        super(c);
        this.galleryId = galleryId;
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public GalleryId getGalleryId() {
        return galleryId;
    }
    
}
