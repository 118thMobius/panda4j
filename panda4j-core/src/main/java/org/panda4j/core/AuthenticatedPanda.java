/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.core;

/**
 *
 * @author mobius
 */
public interface AuthenticatedPanda extends Panda{

    public void addToFavorite(GalleryId id, int folder, String note) throws PandaException;

    public void removeFromFavorite(GalleryId id) throws PandaException;

    public String[] loadFavoriteFoldersTitle() throws PandaException;
    
}
