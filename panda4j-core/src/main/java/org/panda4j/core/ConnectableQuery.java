/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.core;

import java.util.ArrayList;

/**
 *
 * 任意の数だけキーワードやタグを指定して検索できる{@link Query}です。
 *
 * @author mobius
 */
public class ConnectableQuery extends Query {
    
    private final String opt;
    private final ArrayList<ConnectableQueryBuilder.Criterion> criteria;
    private final Genres genres;
    private final int rate;
    private final boolean showExpunged;

    ConnectableQuery(Genres genres, ArrayList<ConnectableQueryBuilder.Criterion> criteria, int rate, boolean showExpanged) {
        if (!genres.isSignificant() && criteria.isEmpty() && showExpanged == false && rate < 2) {
            /**
             * genreが有意でなく Criteriaが指定されておらず showExpangedが無効で
             * rateが2未満の時、オプションが存在していない物と見なして素のURLを返す。
             * 
             */
            this.opt = "";
        } else {
            StringBuilder builder = new StringBuilder(genresString(genres));
            builder.append("f_search=");
            builder.append(generateSearchString(criteria));
            builder.append("&f_apply=Apply Filter&");
            if (showExpanged || rate != 0) {
                builder.append("advsearch=1&f_sname=on&f_stags=on&");
                if (showExpanged) {
                    builder.append("f_sh=on&");
                }
                if (rate != 0) {
                    builder.append("f_sr=on&");
                }
                builder.append("f_srdd=");
                builder.append(Integer.max(2, rate));
            }
            this.opt = builder.toString();
        }
        this.criteria = criteria;
        this.genres = genres;
        this.rate = rate;
        this.showExpunged = showExpanged;
    }

    public ArrayList<ConnectableQueryBuilder.Criterion> getCriteria() {
        return new ArrayList<>(criteria);
    }

    public Genres getGenres() {
        return genres;
    }

    public String getOpt() {
        return opt;
    }

    public int getRate() {
        return rate;
    }
    
    public boolean getShowExpunged(){
        return showExpunged;
    }

    private String generateSearchString(ArrayList<ConnectableQueryBuilder.Criterion> criteria) {
        StringBuilder sb = new StringBuilder();
        for (ConnectableQueryBuilder.Criterion criterion : criteria) {
            sb.append(criterion.getSearchClause());
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    @Override
    public String getUrl(Service service, int page) {
        StringBuilder sb = new StringBuilder(service.getURL());
        if (page != 0) {
            sb.append("?page=");
            sb.append(page);
            if (!opt.isEmpty()) {
                sb.append("&");
                sb.append(encode(opt));
            }
        } else {
            if (!opt.isEmpty()) {
                sb.append("?");
                sb.append(encode(opt));
            }
        }
        return sb.toString();
    }

    private String encode(String raw) {
        return raw
                .replace(" ", "+")
                .replace(":", "%3A")
                .replace("$", "%24");
    }

}
