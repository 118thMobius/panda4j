/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Configオブジェクトのビルダです。
 *
 * @author mobius
 */
public class ConfigurationBuilder {

    /**
     * メモ uconfig=は以下のルール
     *
     * プロパティが-で並んでいる ts_lとdm_tはPanda4J実装に必須 tl_jで日本語タイトル表示
     * xl_以下にxをスプリッタとして各言語の設定が保存されている。
     * 各言語は数字で表示されており、これらの対応はSettingsのソースを見るしかない。
     */
    private static final String CONFIG_KEY = "uconfig";

    private static final String MINIMUM = "ts_l-dm_t";
    private static final String JAPANESE_TITLE = "tl_j";
    private static final String JAPANESE_ONLY = "xl_1x1025x2049x10x1034x2058x20x1044x2068x30x1054x2078x40x1064x2088x50x1074x2098x60x1084x2108x70x1094x2118x80x1104x2128x90x1114x2138x100x1124x2148x110x1134x2158x120x1144x2168x130x1154x2178x254x1278x2302x255x1279x2303";

    private boolean japaneseOnly = false;
    private boolean showJapaneseTitle = false;
    //Serviceが呼び出されるときはAuthenticatedなのでEXモード
    private Service service = Service.EX_HENTAI;
    private Auth auth = null;

    private boolean debug = false;
    private Logger logger = null;

    public ConfigurationBuilder setAuth(Auth auth) {
        this.auth = auth;
        return this;
    }
    
    public ConfigurationBuilder setService(Service service) {
        this.service = service;
        return this;
    }

    public ConfigurationBuilder setJapaneseOnly(boolean japaneseOnly) {
        this.japaneseOnly = japaneseOnly;
        return this;
    }

    public ConfigurationBuilder setShowJapaneseTitle(boolean showJapaneseTitle) {
        this.showJapaneseTitle = showJapaneseTitle;
        return this;
    }

    public ConfigurationBuilder setEnableDebug(boolean enable) {
        this.debug = enable;
        return this;
    }

    public ConfigurationBuilder setLogger(Logger logger) {
        this.logger = logger;
        return this;
    }

    public Configuration build() {
        HashMap<String, String> cookies = new HashMap<>();
        ArrayList<String> options = new ArrayList<>();
        if (japaneseOnly) {
            options.add(JAPANESE_ONLY);
        }
        if (showJapaneseTitle) {
            options.add(JAPANESE_TITLE);
        }
        StringBuilder builder = new StringBuilder(MINIMUM);
        for (String option : options) {
            builder.append("-");
            builder.append(option);
        }
        cookies.put(CONFIG_KEY, builder.toString());
        if (auth != null) {
            //Authから取れるCookieをConfigの内容で上書きせねばならないので、ここは削れない
            HashMap<String, String> c = new HashMap<>(auth.getCookies());
            c.putAll(cookies);
            if (debug) {
                if (logger == null) {
                    return new AuthenticatedConfiguration(auth, c, service, japaneseOnly, showJapaneseTitle, debug);
                } else {
                    return new AuthenticatedConfiguration(auth, c, service, japaneseOnly, showJapaneseTitle, debug, logger);
                }
            }
            return new AuthenticatedConfiguration(auth, c, service, japaneseOnly, showJapaneseTitle);
        } else {
            if (service == Service.EX_HENTAI) {
                throw new IllegalStateException("適切な認証情報が設定されていないとEX-hentaiモードは利用できません。");
            }
            if (debug) {
                if (logger == null) {
                    return new Configuration(cookies, japaneseOnly, showJapaneseTitle, debug);
                } else {
                    return new Configuration(cookies, japaneseOnly, showJapaneseTitle, debug, logger);
                }
            }
            return new Configuration(cookies, japaneseOnly, showJapaneseTitle);
        }
    }
}
