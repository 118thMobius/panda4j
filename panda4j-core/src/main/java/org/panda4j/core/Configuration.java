/*
 * Copyright 2017 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mobius
 */
public class Configuration implements Serializable {
    
    private final HashMap<String,String> cookies = new HashMap<>();
    private final boolean japaneseOnly;
    private final boolean showJapaneseTitle;
    private final boolean debug;
    private final Logger logger;
    
    // TODO:Loggingの実装 
    Configuration(HashMap<String,String> cookies,boolean japaneseOnly,boolean showJapaneseTitle) {
        this(cookies, japaneseOnly, showJapaneseTitle, false);
    }

    Configuration(HashMap<String,String> cookies,boolean japaneseOnly,boolean showJapaneseTitle,boolean debug) {
        this(cookies, japaneseOnly, showJapaneseTitle, debug, Logger.getLogger("Panda4J"));
    }

    Configuration(HashMap<String,String> cookies,boolean japaneseOnly,boolean showJapaneseTitle,boolean debug,Logger logger) {
        this.cookies.putAll(cookies);
        this.japaneseOnly = japaneseOnly;
        this.showJapaneseTitle = showJapaneseTitle;
        this.debug = debug;
        this.logger = logger;
    }

    public HashMap<String, String> getCookies() {
        return cookies;
    }
    
    public String getCookieString(){
        StringBuilder builder = new StringBuilder();
        for (String k : cookies.keySet()) {
            builder.append(k);
            builder.append("=");
            builder.append(cookies.get(k));
            builder.append("; ");
        }
        return builder.toString().substring(0, builder.toString().length()-2);
    }

    public boolean isJapaneseOnly() {
        return japaneseOnly;
    }

    public boolean isShowJapaneseTitle() {
        return showJapaneseTitle;
    }

    public Service getService() {
        return Service.E_HENTAI;
    }
    
    LoggerAccessor getLoggerAccessor(){
        return new LoggerAccessor();
    }
    
    public class LoggerAccessor{

        private LoggerAccessor() {
        }
        
        public void log(Level level,String message){
            if (debug && logger!=null) {
                logger.log(level,message);
            }
        }
    }
}
