/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author mobius
 */
public class SearchResult extends ArrayList<GalleryId>{
    private final Query query;
    private final int length;
    private final int page;

    public SearchResult(Query query,int page ,int length) {
        this.query = query;
        this.page = page;
        this.length = length;
    }

    public SearchResult(Query query,int page, int length, Collection<? extends GalleryId> c) {
        super(c);
        this.query = query;
        this.page = page;
        this.length = length;
    }

    public SearchResult(Query query,int page, int length, int initialCapacity) {
        super(initialCapacity);
        this.query = query;
        this.page = page;
        this.length = length;
    }

    public int getMaxPageNumber() {
        return length;
    }

    public Query getQuery() {
        return query;
    }

    public int getPage() {
        return page;
    }
    
    public boolean hasNext(){
            return length - 1 > page;
    }
}
