/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * アプリケーション全体の設定を扱うクラスです。 ConfigBuilderオブジェクトを通してこのオブジェクトのインスタンスを生成することができます。
 * Parserオブジェクトはこのオブジェクトの持つ値からサービスサイトへのアクセス情報を取得するため、
 * とくにログイン機能を利用するためには正しくこのオブジェクトに値を設定する必要があります。
 *
 * @author mobius
 */
public class AuthenticatedConfiguration extends Configuration {

    private final Service service;
    private final Auth auth;

    protected AuthenticatedConfiguration(Auth auth,HashMap<String ,String> cookies, Service service, boolean japaneseOnly, boolean showJapaneseTitle) {
        super(cookies, japaneseOnly, showJapaneseTitle);
        this.auth = auth;
        this.service = service;
    }

    protected AuthenticatedConfiguration(Auth auth,HashMap<String ,String> cookies, Service service, boolean japaneseOnly, boolean showJapaneseTitle,boolean debug) {
        super(cookies, japaneseOnly, showJapaneseTitle,debug);
        this.auth = auth;
        this.service = service;
    }

    protected AuthenticatedConfiguration(Auth auth,HashMap<String ,String> cookies, Service service, boolean japaneseOnly, boolean showJapaneseTitle,boolean debug,Logger logger) {
        super(cookies, japaneseOnly, showJapaneseTitle,debug,logger);
        this.auth = auth;
        this.service = service;
    }

    /**
     * 利用するサービスを取得します。
     * 
     * @return サービス
     */
    @Override
    public Service getService() {
        return service;
    }

    /**
     * 認証情報を返します。
     * 
     * @return 認証に用いるAuthオブジェクト
     */
    public Auth getAuth() {
        return auth;
    }
}
