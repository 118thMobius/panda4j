/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.Objects;

/**
 * サービスサイトにおける基本的なエントリであるギャラリーを表すオブジェクトである{@link Gallery}をユニークに指すオブジェクトです。
 * このオブジェクトはギャラリーの簡易的な情報を持ちます。
 *
 * @author mobius
 */
public class GalleryId implements Serializable {

    private final String galleryUrl;
    private final String galleryId;
    private final String galleryToken;
    private final String galleryTitle;
    private final float galleryRate;
    private final String galleryThumbnailUrl;
    private final Genre galleryGenre;
    private int favoriteFolderId = -1;

    public GalleryId(String galleryUrl, String galleryId, String galleryToken, String galleryTitle, float rate, String galleryThumbnailUrl, Genre galleryGenre, int favoriteFolderId) {
        this.galleryUrl = galleryUrl;
        this.galleryId = galleryId;
        this.galleryToken = galleryToken;
        this.galleryTitle = galleryTitle;
        this.galleryRate = rate;
        this.galleryThumbnailUrl = galleryThumbnailUrl;
        this.galleryGenre = galleryGenre;
        this.favoriteFolderId = favoriteFolderId;
    }

    /**
     * ギャラリーのタイトルを返します。 ここで取得できる情報は多くの場合英語表記であり、作者名などを含んだ簡易的な情報です。
     *
     * @return ギャラリーのタイトル
     */
    public String getTitle() {
        return galleryTitle;
    }

    /**
     *
     * @return ギャラリーのURL
     */
    public String getUrl() {
        return galleryUrl;
    }

    /**
     *
     * @return サムネイルのURL
     */
    public String getThumbnailUrl() {
        return galleryThumbnailUrl;
    }

    /**
     *
     * @return ギャラリーの評価
     */
    public float getRate() {
        return galleryRate;
    }

    /**
     *
     * @return ギャラリーを指すId
     */
    public String getId() {
        return galleryId;
    }

    /**
     * 
     * @return ギャラリーを指すToken
     */
    public String getToken() {
        return galleryToken;
    }

    /**
     * 
     * @return ギャラリーのジャンル
     */
    public Genre getGenre() {
        return galleryGenre;
    }

    public boolean isFavorited() {
        return favoriteFolderId != -1;
    }

    public int getFavoriteFolderId() {
        return favoriteFolderId;
    }

    public void setFavoriteFolderId(int favoriteFolderId) {
        this.favoriteFolderId = favoriteFolderId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GalleryId) {
            GalleryId id = (GalleryId) obj;
            if (id.galleryId.equals(this.galleryId) && id.galleryToken.equals(this.galleryToken)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.galleryId);
        hash = 53 * hash + Objects.hashCode(this.galleryToken);
        return hash;
    }
}
