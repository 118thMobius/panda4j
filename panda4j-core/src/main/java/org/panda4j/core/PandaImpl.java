/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import org.panda4j.core.parser.ParseException;
import org.panda4j.core.parser.Parser;
import org.panda4j.core.util.PandaLoader;

/**
 *
 * @author mobius
 */
class PandaImpl implements Panda {

    protected final Parser parser;
    protected final Configuration config;

    PandaImpl(Parser parser, Configuration config) {
        this.parser = parser;
        this.config = config;
    }
    
    @Override
    public SearchResult search(Query query, int page) throws PandaException {
        if (query == null) {
            query = new SearchQueryBuilder().build();
        }
        try {
            return parser.parseSearchResult(query, page);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Override
    public Gallery loadGallery(GalleryId id) throws PandaException {
        try {
            return parser.parseGallery(id);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Override
    public Gallery loadGallery(String url) throws PandaException {
        try {
            return parser.parseGallery(url);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Override
    public GalleryPage loadGalleryPage(GalleryId id, int page) throws PandaException {
        try {
            return parser.parseGalleryPage(id, page);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Deprecated
    @Override
    public GalleryPage loadGalleryPage(Gallery gallery, int page) throws PandaException {
        try {
            return parser.parseGalleryPage(gallery, page);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Override
    public Media loadMedia(MediaId id) throws PandaException {
        try {
            return parser.parseMedia(id);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Override
    public Media reLoadMedia(Media media) throws PandaException {
        try {
            return parser.reLoadMedia(media);
        } catch (ParseException ex) {
            throw new PandaException(ex);
        }
    }

    @Override
    public PandaLoader getPandaLoader() {
        return new PandaLoader(config);
    }
}
