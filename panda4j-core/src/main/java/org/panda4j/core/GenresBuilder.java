/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.util.Arrays;
import java.util.HashSet;

/**
 *
 * @author mobius
 */
public class GenresBuilder {
    private final HashSet<Genre> genres = new HashSet<>();
    
    public GenresBuilder enable(Genre genre){
        this.genres.add(genre);
        return this;
    }
    
    public GenresBuilder disable(Genre genre){
        this.genres.remove(genre);
        return this;
    }
    
    public GenresBuilder enableAll(){
        this.genres.addAll(Arrays.asList(Genre.values()));
        return this;
    }
    
    public GenresBuilder enableAll(Genre... genres){
        this.genres.addAll(java.util.Arrays.asList(genres));
        return this;
    }
    
    public GenresBuilder disableAll(){
        this.genres.clear();
        return this;
    }
    
    public GenresBuilder disableAll(Genre... genres){
        this.genres.removeAll(java.util.Arrays.asList(genres));
        return this;
    }
    
    public GenresBuilder enableOnly(Genre genre){
        this.genres.clear();
        this.genres.add(genre);
        return this;
    }
    
    public GenresBuilder enableExcept(Genre genre){
        enableAll();
        this.genres.remove(genre);
        return this;
    }
    
    public Genres build(){
        return new Genres(this.genres);
    }
}
