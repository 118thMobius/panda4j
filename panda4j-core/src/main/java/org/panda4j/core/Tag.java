/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.Objects;

/**
 * ギャラリーに対して付与されるタグの情報を持つクラスです。
 * これらは文字列でのタグ名を持ち、{@link org.panda4j.core.TagType}によって分類されます。
 *
 * @author mobius
 */
public class Tag implements Serializable{
    private final String tagString;
    private final TagType tagType;

    public Tag(String tagString, TagType tagType) {
        this.tagString = tagString;
        this.tagType = tagType;
    }

    public String getTagString() {
        return tagString;
    }

    public TagType getTagType() {
        return tagType;
    }

    @Override
    public String toString() {
        return new StringBuilder(tagType.name()).append("%").append(tagString).toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj instanceof Tag) {
                Tag tag = (Tag) obj;
                if (tag.tagType == this.tagType) {
                    return tag.tagString.equals(tagString);
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.tagString);
        hash = 47 * hash + Objects.hashCode(this.tagType);
        return hash;
    }
}
