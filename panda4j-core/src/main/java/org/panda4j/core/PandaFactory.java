/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import org.panda4j.core.parser.EParser;
import org.panda4j.core.parser.ExParser;

/**
 *
 * @author mobius
 */
public class PandaFactory {

    private Configuration config = null;

    public PandaFactory setConfig(Configuration config) {
        this.config = config;
        return this;
    }

    public Panda build() throws PandaFactoryException {
        if (config != null) {
            if (config instanceof AuthenticatedConfiguration) {
                return new AuthenticatedPandaImpl(new ExParser((AuthenticatedConfiguration) config,config.getLoggerAccessor()), config);
            } else {
                return new PandaImpl(new EParser(config,config.getLoggerAccessor()), config);
            }
        }
        throw new IllegalStateException("Configがセットされていません。");
    }
}
