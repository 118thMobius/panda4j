/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ギャラリーに対して投稿されたコメントを扱うクラスです。
 *
 * @author mobius
 */
public class Comment implements Serializable {

    private final Date date;
    private final String rawContent;
    private final String content;
    private final ArrayList<String> urlList;
    private final String name;

    public Comment(Date date, String name, String rawContent) {
        this.date = date;
        this.rawContent = rawContent;
        this.urlList = new ArrayList<>();
        Pattern p = Pattern.compile("<a href=.*?>");
        Matcher matcher = p.matcher(rawContent);
        while (matcher.find()) {
            this.urlList.add(matcher.group());
        }
        this.content = rawContent.replace("<br>", "\n").replaceAll("<.*>", "").trim();
        this.name = name;
    }

    /**
     * HTMLタグを含めたコメントを取得します。
     *
     * @return
     */
    public String getRawContent() {
        return rawContent;
    }

    /**
     * コメントからHTMLタグを除いたものを取得します。 タグが必要な場合は#getRawContentから取得できます。
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     * 投稿者の名前を取得します。
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * コメントされた日時を返します。
     *
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     * コメントに含まれたリンクを取得します。
     *
     * @return
     */
    public ArrayList<String> getUrlList() {
        return urlList;
    }
}
