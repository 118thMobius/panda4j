/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author mobius
 */
public final class Authenticator {

    private static final String LOGIN_URL = "https://forums.e-hentai.org/index.php?act=Login&CODE=01";
    private static final String THUMBNAIL_URL = "https://exhentai.org/?inline_set=dm_t";
    private final boolean debug;
    private final Logger logger;

    public Authenticator() {
        this.debug = false;
        logger = null;
    }

    public Authenticator(boolean debug) {
        this.debug = debug;
        this.logger = Logger.getLogger("Authenticator");
    }

    public Authenticator(boolean debug,Logger logger) {
        this.debug = debug;
        this.logger = logger;
    }
    
    private void info(String msg){
        if (debug) {
            logger.info(msg);
        }
    }
    
    private void severe(String msg){
        if (debug) {
            logger.severe(msg);
        }
    }

    public final Auth authentication(String userName, String passWord) throws AuthException {
        try {
            info("1段階目開始。");
            Response response = Jsoup.connect(LOGIN_URL)
                    .data("CookieDate", "1")
                    .data("b", "d")
                    .data("bt", "")
                    .data("UserName", userName)
                    .data("PassWord", passWord)
                    .data("ipb_login_submit", "Login!")
                    .method(Connection.Method.POST)
                    .execute();
            Map<String, String> cookies = response.cookies();
            info("HTTPステータスコード: "+response.statusCode()+response.statusMessage());
            info("1段階目終了。Cookie: " + cookies);
            response = Jsoup.connect(THUMBNAIL_URL).userAgent("Mozzila").cookies(cookies).execute();
            cookies.putAll(response.cookies());
            info("1.5段階目終了。Cookie: " + cookies);
            //検索
            Query query = new SearchQueryBuilder().build();
            response = Jsoup.connect(query.getUrl(Service.EX_HENTAI, 0)).userAgent("Mozilla").cookies(cookies).execute();
            cookies.putAll(response.cookies());
            info("2段階目終了。Cookie: " + cookies);
            Document document = response.parse();
            Element element = document.getElementsByClass("id1").first();
            String galleryUrl = element.getElementsByClass("id2").first().child(0).attr("href").trim();
            //ギャラリー
            response = Jsoup.connect(galleryUrl).userAgent("Mozilla").cookies(cookies).execute();
            cookies.putAll(response.cookies());
            info("3段階目終了。Cookie: " + cookies);
            return new Auth(userName, cookies);
        } catch (IOException ex) {
            severe("認証に失敗しました。");
            throw new AuthException("認証に失敗しました", ex);
        }
    }
}
