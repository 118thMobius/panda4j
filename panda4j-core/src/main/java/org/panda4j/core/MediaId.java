/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author mobius
 */
public class MediaId implements Serializable {

    private final String mediaUrl;
    private final String mediaThumbnailUrl;
    private final GalleryId galleryId;
    private final int mediaNumber;

    public MediaId(String mediaUrl, String mediaThumbnailUrl, GalleryId galleryId, int mediaNumber) {
        this.mediaUrl = mediaUrl;
        this.mediaThumbnailUrl = mediaThumbnailUrl;
        this.galleryId = galleryId;
        this.mediaNumber = mediaNumber;
    }

    @Override
    public String toString() {
        return mediaUrl;
    }

    /**
     * このメディアのURLを取得します。
     *
     * @return メディアのURL
     */
    public String getMediaUrl() {
        return mediaUrl;
    }

    /**
     * このメディアの概要を表すサムネイルのURLを取得します。
     *
     * @return サムネイルのURL
     */
    public String getMediaThumbnailUrl() {
        return mediaThumbnailUrl;
    }

    /**
     * このメディアがギャラリー中何番目かを取得します。
     *
     * @return ギャラリー内で一意な番号
     */
    public int getMediaNumber() {
        return mediaNumber;
    }

    /**
     * このメディアを含むギャラリーをロードするのに使ったGalleryIdを取得します。
     *
     * @return ギャラリーID
     */
    public GalleryId getGalleryId() {
        return galleryId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MediaId) {
            MediaId mediaId = (MediaId) obj;
            if (this.galleryId.equals(mediaId.galleryId) && this.mediaNumber == mediaId.mediaNumber) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.galleryId);
        hash = 97 * hash + this.mediaNumber;
        return hash;
    }
}
