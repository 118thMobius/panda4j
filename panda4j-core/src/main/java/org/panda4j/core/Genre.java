/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

/**
 *
 * @author mobius
 */
public enum Genre {
    DOUJINSHI("doujinshi"),
    MANGA("manga"),
    ARTIST("artistcg"),
    GAME("gamecg"),
    WESTERN("western"),
    NONH("non-h"),
    IMAGE("imageset"),
    COSPLAY("cosplay"),
    ASIAN("asianporn"),
    MISC("misc");
    private final String searchKeyWord;

    private Genre(String keyword) {
        this.searchKeyWord = keyword;
    }
    
    public String getSearchKeyword(){
        return searchKeyWord;
    }
    
    public static Genre getGenreByName(String str) {
        switch (str.toUpperCase()) {
            case "DOUJINSHI":
                return DOUJINSHI;
            case "MANGA":
                return MANGA;
            case "ARTIST":
                return ARTIST;
            case "GAME":
            case "GAMECG":
                return GAME;
            case "WESTERN":
                return WESTERN;
            case "NONH":
            case "NON-H":
                return NONH;
            case "IMAGE":
            case "IMAGESET":
                return IMAGE;
            case "COSPLAY":
                return COSPLAY;
            case "ASIAN":
                return ASIAN;
            case "MISC":
            default:
                return MISC;
        }
    }
    
}
