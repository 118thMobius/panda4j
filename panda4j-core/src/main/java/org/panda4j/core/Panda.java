/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import org.panda4j.core.util.PandaLoader;

/**
 *
 * @author mobius
 */
public interface Panda {

    public SearchResult search(Query query, int page) throws PandaException;

    public Gallery loadGallery(GalleryId id) throws PandaException;
    
    public Gallery loadGallery(String url) throws PandaException;

    public GalleryPage loadGalleryPage(GalleryId id, int page) throws PandaException;

    @Deprecated
    public GalleryPage loadGalleryPage(Gallery gallery, int page) throws PandaException;
    
    public Media loadMedia(MediaId id) throws PandaException;

    public Media reLoadMedia(Media media) throws PandaException;

    public PandaLoader getPandaLoader();
}
