/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

//TODO:JavaDoc
/**
 * 
 * 2.2.0まで標準の検索手段でしたが、2.2.1から{@link ConnectableQuery}に置換されました。
 *
 * @author mobius
 */
@Deprecated
public class SearchQueryBuilder implements QueryBuilder{
    
    private int rate = 0;
    private boolean showExpanged = false;
    private Tag tag;
    private String keyword = "";
    private SearchType searchType = SearchType.NONE;
    private Genres genre = new GenresBuilder().enableAll().build();

    public SearchQueryBuilder setShowExpanged(boolean opt) {
        showExpanged = opt;
        return this;
    }

    public SearchQueryBuilder setKeyWord(String keyword) {
        this.keyword = keyword;
        searchType = SearchType.KEYWORDS;
        return this;
    }

    public SearchQueryBuilder setTag(Tag tag) {
        this.tag = tag;
        searchType = SearchType.TAG;
        return this;
    }

    public SearchQueryBuilder setMinimumRate(int rate) {
        this.rate = rate;
        return this;
    }

    public SearchQueryBuilder setGenre(Genres genre) {
        this.genre = genre;
        return this;
    }

    @Override
    public Query build() {
        switch (searchType) {
            case NONE:
            default:
                return new KeyWordQuery("", genre, rate, showExpanged);
            case KEYWORDS:
                return new KeyWordQuery(keyword, genre, rate, showExpanged);
            case TAG:
                return new TagQuery(tag, genre, rate, showExpanged);
        }
    }

    private enum SearchType {
        NONE,
        KEYWORDS,
        TAG,
    }
}
