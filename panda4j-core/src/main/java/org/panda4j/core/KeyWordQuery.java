/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

/**
 *
 * @author mobius
 */
@Deprecated
class KeyWordQuery extends Query{
    private final String url;

    KeyWordQuery(String keyword, Genres genre, int rate, boolean showExpanged) {
        StringBuilder builder = new StringBuilder("?");
        builder.append(genresString(genre));
        builder.append("f_search=");
        if (keyword.contains(" ")) {
            builder.append(keyword.replace(" ", "+"));
        } else {
            builder.append(keyword);
        }
        builder.append("&f_apply=Apply+Filter&");
        if (showExpanged || rate != 0) {
            builder.append("advsearch=1&f_sname=on&f_stags=on&");
            if (showExpanged) {
                builder.append("f_sh=on&");
            }
            if (rate != 0) {
                builder.append("f_sr=on&f_srdd=");
                builder.append(rate);
                builder.append("&");
            } else {
                builder.append("f_srdd=2&");
            }
        }
        this.url = builder.toString();
    }

    @Override
    public String getUrl(Service service,int page) {
        if (page != 0) {
            return service.getURL()+url + "page=" + page;
        } else {
            return service.getURL()+url;
        }
    }
}
