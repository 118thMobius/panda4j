/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * サービスサイトにおける基本的なエントリであるギャラリーを表すオブジェクトです。
 * ギャラリーは{@link GalleryId}によって単一的に取得されます。 このオブジェクトは取得時点で1ページのギャラリーページを含みます。
 *
 * @author mobius
 */
public class Gallery extends MediaIds implements Serializable {
    private final GalleryId galleryId;
    private final ArrayList<Tag>[] tagGroups = new ArrayList[TagType.values().length];
    private ArrayList<Comment> comments = new ArrayList<>();
    private final String title;
    private final String showTitle;
    private final int length;
    private final float rate;
    
    public Gallery(GalleryId galleryId,String title, String showTitle, int length, float rate, MediaIds mediaIds, ArrayList<Tag>[] tagGroups, ArrayList<Comment> comments) {
        super(mediaIds);
        this.galleryId = galleryId;
        this.showTitle = showTitle;
        this.title = title;
        this.length = length;
        this.rate = rate;
        System.arraycopy(tagGroups, 0, this.tagGroups, 0, tagGroups.length);
        this.comments = comments;
    }

    /**
     * ギャラリーページを追加します。
     * 
     * @param galleryPage ギャラリーのページ
     */
    public synchronized void addGalleryPage(GalleryPage galleryPage) {
        addAll(galleryPage);
    }

    public GalleryId getGalleryId() {
        return galleryId;
    }

    /**
     * 
     * @return ギャラリーを表すId
     */
    public String getId() {
        return galleryId.getId();
    }

    /**
     * 
     * @return ギャラリーを表すToken
     */
    public String getToken() {
        return galleryId.getToken();
    }

    /**
     * ギャラリーのURL
     * @return 
     */
    public String getThumbnailUrl() {
        return galleryId.getThumbnailUrl();
    }

    /**
     * 
     * @return ギャラリーのタイトル
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @return ギャラリーの日本語タイトル
     */
    public String getShowTitle() {
        if (showTitle.isEmpty()) {
            return title;
        }
        return showTitle;
    }

    /**
     * 
     * @return ギャラリーのURL
     */
    public String getUrl() {
        return galleryId.getUrl();
    }

    /**
     * 
     * @return ギャラリーのメディアの数
     */
    @Deprecated
    public int getLength() {
        return length;
    }
    
    public int getNumberOfAllMedias() {
        return length;
    }
    
    public int getNumberOfLoadedMedias() {
        return size();
    }
    
    /**
     * このギャラリーの評価を返します。
     * {@link GalleryId}は0.5単位で評価を返すのに対し、このメソッドは0.1単位で評価を返します。
     * そのためこれらは必ずしも同一ではない可能性があります。
     * 
     * @return このギャラリーの評価
     */
    public float getRate() {
        return rate;
    }

    /**
     * 
     * @return ギャラリーのページ数
     */
    @Deprecated
    public int getLengthOfPage() {
        return (length + 19) / 20;
    }
    
    public int getNumberOfAllPage() {
        return (length + 19) / 20;
    }
    
    public int getNumberOfLoadedPage() {
        return (size() + 19) / 20;
    }

    /**
     * 
     * @return ギャラリーのジャンル
     */
    public Genre getGenre() {
        return galleryId.getGenre();
    }

    /**
     * 
     * @return ギャラリーをお気に入り登録しているかどうか
     */
    public boolean isFavorited() {
        return galleryId.isFavorited();
    }

    /**
     * 
     * @return ギャラリーの登録されているお気に入りフォルダ
     */
    public int getFavoriteFolderId() {
        return galleryId.getFavoriteFolderId();
    }

    /**
     * 
     * @return ギャラリーにつけられたタググループの配列
     */
    public ArrayList<Tag>[] getTagGroups() {
        return tagGroups;
    }
    
    /**
     * 
     * @param type 取得するタググループのタイプ
     * @return タググループ
     */
    public ArrayList<Tag> getTags(TagType type) {
        return tagGroups[type.ordinal()];
    }
    /**
     * 
     * @return コメントのList
     */
    public ArrayList<Comment> getComments() {
        return comments;
    }
}
