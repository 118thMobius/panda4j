/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author mobius
 */
public class Media implements Serializable{
    private final String url;
    private final MediaId id;
    private final String reLoadParam;
    private final boolean alternative;
    
    
    public Media(String url, MediaId id,String reloadParam) {
        this(url, id, reloadParam, false);
    }
    
    public Media(String url, MediaId id,String reloadParam,boolean alternative) {
        this.url = url;
        this.id = id;
        this.reLoadParam = reloadParam;
        this.alternative = alternative;
    }

    public MediaId getMediaId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getReLoadParam() {
        return reLoadParam;
    }
    
    public boolean isAlternative(){
        return alternative;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Media) {
            Media media =  (Media) obj;
            if (this.id.equals(media.id)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    
}
