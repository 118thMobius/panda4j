/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;

/**
 *
 * @author mobius
 */
public class Genres implements Serializable{
    private final HashSet<Genre> genres;
    
    Genres(HashSet<Genre> genres) {
        this.genres = new HashSet<>(genres);
    }

    public HashSet<Genre> getGenres() {
        return new HashSet<>(genres);
    }
    
    public boolean isSignificant(){
        return !(new GenresBuilder().build().equals(this) || new GenresBuilder().enableAll().build().equals(this));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Genres other = (Genres) obj;
        if (!Objects.equals(this.genres, other.genres)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.genres);
        return hash;
    }
}
