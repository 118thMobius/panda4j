/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;

/**
 * ギャラリーの検索する際に利用する条件を扱う抽象クラスです。 このクラスを継承したクラスによって検索を行うことができます。
 *
 * @author mobius
 */
public abstract class Query implements Serializable {

    /**
     * 検索URLを取得します。
     *
     * @param service 検索に使うサービスタイプ。
     * @param page 検索するページ数。
     * @return 検索に使うURL
     */
    public abstract String getUrl(Service service, int page);

    public static Query getDefault() {
        return new Query() {
            @Override
            public String getUrl(Service service, int page) {
                if (page != 0) {
                    return service.getURL() + "?page=" + page;
                } else {
                    return service.getURL();
                }
            }
        };
    }

    protected static String genresString(Genres genres) {
        StringBuilder builder = new StringBuilder();
        for (Genre g : Genre.values()) {
            builder.append("f_");
            builder.append(g.getSearchKeyword());
            if (genres.getGenres().contains(g)) {
                builder.append("=1&");
            } else {
                builder.append("=0&");
            }
        }
        return builder.toString();
    }
}
