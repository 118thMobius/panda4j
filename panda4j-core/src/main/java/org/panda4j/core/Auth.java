/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 認証情報を保持するクラスです。
 * 内部的にはサービスサイトのCookieを保持しており、Authenticator以外の方法でこれらを取得してもAPIをコールできます。
 *
 * @author mobius
 */
public final class Auth implements Serializable{
    private final String userName;
    private final HashMap<String,String> cookies = new HashMap<>();
    
    public Auth(String userName,String cookie){
        this.userName = userName;
        for (String str : cookie.split(";")) {
            this.cookies.put(str.split("=")[0], str.split("=")[1]);
        }
    }

    public Auth(String userName,Map<String, String> cookies) {
        this.userName = userName;
        this.cookies.putAll(cookies);
    }

    /**
     * サービスサイトの認証情報を含んだCookieを返します。
     * 
     * @return 内部に保持しているCookie
     */
    public HashMap<String, String> getCookies() {
        return cookies;
    }
    
    /**
     * {@link #getCookies() }で取得したCookie情報をHTTPリクエストで用いられる形に文字列としてフォーマットしたものを返します。
     * 
     * @return Cookie文字列
     */
    public String getCookieString(){
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : cookies.entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append(";");
        }
        return builder.toString();
    }

    /**
     * このAuthインスタンスによって認証されるユーザーの名前を取得します。
     * 
     * @return ユーザー名
     */
    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "Auth:"+userName;
    }
}
