/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.panda4j.core.Configuration;

/**
 * サービスサイトの提供するリソースの読み込みを行うためのユーティリティクラスです。
 *
 * @author mobius
 */
public class PandaLoader {

    private final Configuration config;

    public PandaLoader(Configuration config) {
        this.config = config;
    }

    /**
     * サービスサイト上のリソースを読み込むことができるようにHttpUrlConnectionを生成します。
     * 
     * @param url
     * @return 
     */
    public HttpURLConnection getResourceAsHttpURLConnection(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestProperty("Cookie", config.getCookieString());
            return connection;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * 
     * @param url
     * @return 
     */
    public InputStream getResourceAsStream(String url) {
        try {
            return getResourceAsHttpURLConnection(url).getInputStream();
        } catch (IOException ex) {
            return null;
        }
    }
}
