/*
 * Copyright 2017 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core.parser;

import org.panda4j.core.GalleryId;

/**
 * AuthenticatedParserは認証の必要な機能へのアクセスを提供します。
 * 現在のサービスサイトの仕様では、これらはお気に入りに関する操作に限られます。
 *
 * @author mobius
 */
public interface AuthenticatedParser extends Parser{

    /**
     * ギャラリーをお気に入りに加えます。
     * お気に入りフォルダと、お気に入りに対する非公開のコメントを指定することができます。
     * 
     * @param id お気に入りに加えるギャラリーを示す{@link org.panda4j.core.GalleryId}
     * @param favoriteFolder お気に入りフォルダを示すint値
     * @param note お気に入りに対するコメント
     * @throws ParseException 
     */
    public void addToFavorite(GalleryId id, int favoriteFolder, String note) throws ParseException;
    
    /**
     * ギャラリーをお気に入りから外します。
     * 
     * @param id お気に入りから除外するギャラリーを示す{@link org.panda4j.core.GalleryId}
     * @throws ParseException 
     */
    public void removeFromFavorite(GalleryId id) throws ParseException;
    
    /**
     * ユーザーがお気に入りフォルダに対して付与した名前を取得します。
     * 
     * @return
     * @throws ParseException 
     */
    public String[] parseFavoriteFoldersTitle() throws ParseException;
    
}
