/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core.parser;

import org.panda4j.core.Query;
import org.panda4j.core.Gallery;
import org.panda4j.core.GalleryId;
import org.panda4j.core.GalleryPage;
import org.panda4j.core.Media;
import org.panda4j.core.MediaId;
import org.panda4j.core.SearchResult;

/**
 * Parserは認証なしで扱える機能へのアクセスを提供します。
 * 認証の必要な機能へアクセスするには、Parserを継承した{@link org.panda4j.core.parser.AuthenticatedParser}を利用してください。
 * 
 * @author mobius
 */
public interface Parser {

    public SearchResult parseSearchResult(Query query,int page) throws ParseException;

    /**
     * idに与えられた{@link org.panda4j.core.GalleryId}に示されたギャラリーの詳細を含む{@link org.panda4j.core.Gallery}を返します。
     * @param id 取得するギャラリーを示す{@link org.panda4j.core.GalleryId}
     * @return ギャラリーの詳細
     * @throws ParseException パース中に例外が発生した場合にスローされます
     */
    public Gallery parseGallery(GalleryId id) throws ParseException;
    
    public Gallery parseGallery(String url) throws ParseException;

    /**
     * 与えられた{@link org.panda4j.core.GalleryId}の示すギャラリーのうち、指定されたページの{@link org.panda4j.core.MediaId}を含む{@link org.panda4j.core.GalleryPage}を返します。
     * 
     * @param id 取得するギャラリーを示す{@link org.panda4j.core.GalleryId}
     * @param page 取得するページ(最初のページは0)
     * @return 指定されたページの{@link org.panda4j.core.MediaId}配列を含む{@link org.panda4j.core.GalleryPage}
     * @throws ParseException パース中に例外が発生した場合にスローされます。
     */
    public GalleryPage parseGalleryPage(GalleryId id, int page) throws ParseException;

    /**
     * 与えられた{@link org.panda4j.core.Gallery}のうち、指定されたページの{@link org.panda4j.core.MediaId}を含む{@link org.panda4j.core.GalleryPage}を返します。
     * このAPIに与えられた{@link org.panda4j.core.Gallery}は暗黙のうちにこのAPIによって返される{@link org.panda4j.core.GalleryPage}を内部に与えられます。
     * 
     * @param gallery 取得するギャラリー
     * @param page 取得するページ(最初のページは0)
     * @return 指定されたページの{@link org.panda4j.core.MediaId}配列を含む{@link org.panda4j.core.GalleryPage}
     * @throws ParseException パース中に例外が発生した場合にスローされます。
     * @deprecated このAPIではすでに取得済みの{@link org.panda4j.core.Gallery}の扱いが不明瞭であるため、推奨されなくなりました。代わりに{@link #parseGalleryPage(org.panda4j.core.GalleryId, int) }を利用してください。
     */
    @Deprecated
    public GalleryPage parseGalleryPage(Gallery gallery, int page) throws ParseException;

    /**
     * {@link org.panda4j.core.MediaId}の示すメディアの実体である{@link org.panda4j.core.Media}を取得します。
     * 
     * @param id 取得するメディアを示す{@link org.panda4j.core.MediaId}
     * @return 与えられた{@link org.panda4j.core.MediaId}の実体
     * @throws ParseException 
     */
    public Media parseMedia(MediaId id) throws ParseException;

    public Media reLoadMedia(Media media) throws ParseException;
}
