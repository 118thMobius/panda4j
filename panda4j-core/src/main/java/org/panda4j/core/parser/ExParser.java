/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core.parser;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.panda4j.core.AuthenticatedConfiguration;
import org.panda4j.core.Configuration;
import org.panda4j.core.GalleryId;

/**
 *
 * @author mobius
 */
public class ExParser extends EParser implements AuthenticatedParser{

    private final AuthenticatedConfiguration config;

    public ExParser(AuthenticatedConfiguration config,Configuration.LoggerAccessor loggerAccessor) {
        super(config,loggerAccessor);
        this.config = config;
    }

    @Override
    public void addToFavorite(GalleryId id, int favoriteFolder, String note) throws ParseException {
        try {
            loggerAccessor.log(Level.INFO,"お気に入りに追加");
            StringBuilder builder = new StringBuilder(config.getService().getURL());
            builder.append("gallerypopups.php?gid=");
            builder.append(id.getId());
            builder.append("&t=");
            builder.append(id.getToken());
            builder.append("&act=addfav");
            HashMap<String, String> param = new HashMap<>();
            param.put("favcat", String.valueOf(favoriteFolder));
            param.put("favnote", note);
            param.put("submit", "Apply+Changes");
            Connection.Response response = Jsoup.connect(builder.toString()).userAgent("Moziila").cookies(config.getCookies())
                    .data(param)
                    .method(Connection.Method.POST)
                    .execute();
            System.out.println(response.parse());
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    @Override
    public void removeFromFavorite(GalleryId id) throws ParseException {
        try {
            loggerAccessor.log(Level.INFO,"お気に入りから削除");
            StringBuilder builder = new StringBuilder(config.getService().getURL());
            builder.append("gallerypopups.php?gid=");
            builder.append(id.getId());
            builder.append("&t=");
            builder.append(id.getToken());
            builder.append("&act=addfav");
            HashMap<String, String> param = new HashMap<>();
            param.put("favcat", "favdel");
            param.put("favnote", "");
            param.put("submit", "Apply+Changes");
            Connection.Response response = Jsoup.connect(builder.toString()).userAgent("Moziila").cookies(config.getCookies())
                    .data(param)
                    .method(Connection.Method.POST)
                    .execute();
            System.out.println(response.parse());
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    @Override
    public String[] parseFavoriteFoldersTitle() throws ParseException {
        String[] titles = new String[10];
        try {
            loggerAccessor.log(Level.INFO,"お気に入りタイトルを取得");
            Connection.Response response = Jsoup.connect(config.getService().getURL() + "/favorites.php").userAgent("Mozilla").cookies(config.getCookies()).execute();
            Document document = response.parse();
            Element favoriteFolderParent = document.getElementsByClass("nosel").first();
            Elements favoriteFolders = favoriteFolderParent.getElementsByClass("fp");
            for (int i = 0; i < 10; i++) {
                titles[i] = favoriteFolders.get(i).child(2).text();
            }
            return titles;
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }
}
