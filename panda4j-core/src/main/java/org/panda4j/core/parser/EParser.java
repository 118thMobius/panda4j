/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core.parser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.regex.Pattern;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.panda4j.core.Comment;
import org.panda4j.core.Configuration;
import org.panda4j.core.Gallery;
import org.panda4j.core.GalleryId;
import org.panda4j.core.GalleryPage;
import org.panda4j.core.Genre;
import org.panda4j.core.Media;
import org.panda4j.core.MediaId;
import org.panda4j.core.MediaIds;
import org.panda4j.core.Query;
import org.panda4j.core.SearchResult;
import org.panda4j.core.Tag;
import org.panda4j.core.TagType;

/**
 *
 * @author mobius
 */
public class EParser implements Parser {

    private final Configuration config;
    protected final Configuration.LoggerAccessor loggerAccessor;

    public EParser(Configuration config,Configuration.LoggerAccessor loggerAccessor) {
        this.config = config;
        this.loggerAccessor = loggerAccessor;
    }

    @Override
    public SearchResult parseSearchResult(Query query, int page) throws ParseException {
        try {
            loggerAccessor.log(Level.INFO,"検索開始:" + query.getUrl(config.getService(), page));
            Connection.Response response = Jsoup.connect(query.getUrl(config.getService(), page)).userAgent("Mozilla").cookies(config.getCookies()).execute();
            loggerAccessor.log(Level.INFO,"レスポンス受信");
            Document document = response.parse();
            if (document.getElementsByClass("ptt").first() == null) {
                throw new ParseException("一致するギャラリーがありません");
            }
            Elements lengthElements = document.getElementsByClass("ptt").first().child(0).child(0).children();
            int length = 0;
            for (Element lengthElement : lengthElements) {
                if (Pattern.compile("^[0-9]*$").matcher(lengthElement.text()).find()) {
                    length = Math.max(length, Integer.valueOf(lengthElement.text()));
                }
            }
            SearchResult result = new SearchResult(query, page, length);
            Elements elements = document.getElementsByClass("id1");
            loggerAccessor.log(Level.INFO,"エントリー数:"+elements.size());
            for (Element element : elements) {
                Element title = element.getElementsByClass("id2").first().child(0);
                Element image = element.getElementsByClass("id3").first().child(0).child(0);
                Element genre = element.getElementsByClass("id4").first().child(0);
                Element favorite = element.getElementsByClass("id44").first().child(0);
                Element rate = element.getElementsByClass("id43").first();

                String galleryTitle = title.text();
                String galleryUrl = title.attr("href").trim();
                String galleryThumbnailUrl = image.attributes().get("src").trim();
                String galleryId = galleryUrl.substring(galleryUrl.indexOf("/g/") + 3).split("/")[0];
                String galleryToken = galleryUrl.substring(galleryUrl.indexOf("/g/") + 3).split("/")[1];
                String rateString = rate.attr("style");
                float integerOfRate = Float.valueOf(rateString.substring(rateString.indexOf("background-position:") + "background-position:".length(), rateString.indexOf("px"))) / 16;
                float decimalOfRate = (Float.valueOf(rateString.substring(rateString.indexOf("px") + "px".length(), rateString.indexOf("px;"))) + 1) / 40;
                
                float galleryRate = 5 + integerOfRate + decimalOfRate;
                int favoriteFolderId = -1;
                for (Element e : favorite.children()) {
                    if (e.tag() == org.jsoup.parser.Tag.valueOf("div")) {
                        String f = e.attributes().get("style");
                        f = f.substring(f.indexOf("background-position"), f.indexOf("cursor"));
                        int n = Integer.valueOf(f.substring(f.indexOf("px -") + 4, f.indexOf("px;")));
                        favoriteFolderId = (n - 2) / 19;
                    }
                }
                GalleryId id = null;
                switch (genre.attributes().get("title").trim()) {
                    case "Doujinshi":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.DOUJINSHI, favoriteFolderId);
                        break;
                    case "Manga":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.MANGA, favoriteFolderId);
                        break;
                    case "Artist CG Sets":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.ARTIST, favoriteFolderId);
                        break;
                    case "Game CG Sets":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.GAME, favoriteFolderId);
                        break;
                    case "Western":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.WESTERN, favoriteFolderId);
                        break;
                    case "Non-H":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.NONH, favoriteFolderId);
                        break;
                    case "Image Sets":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.IMAGE, favoriteFolderId);
                        break;
                    case "Cosplay":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.COSPLAY, favoriteFolderId);
                        break;
                    case "Asian Porn":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.ASIAN, favoriteFolderId);
                        break;
                    case "Misc":
                        id = new GalleryId(galleryUrl, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, Genre.MISC, favoriteFolderId);
                        break;
                }
                result.add(id);
            }
            return result;
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    private Gallery privateParseGallery(GalleryId id, Connection.Response response) throws ParseException {
        try {
            loggerAccessor.log(Level.INFO,"ギャラリー読み込み開始:" + id.getUrl());
            MediaIds mediaIds = new MediaIds();
            Document document = response.parse();
            
            //MediaId配列パース
            Elements mediaElements = document.getElementsByClass("gdtl");
            int number = 0;
            for (Element element : mediaElements) {
                Element e = element.child(0);
                mediaIds.add(new MediaId(
                        e.attributes().get("href"),
                        e.child(0).attributes().get("src"),
                        id,
                        ++number
                ));
            }
            //コメント配列パース
            ArrayList<Comment> comments = new ArrayList<>();
            Elements commentElements = document.getElementById("cdiv").getElementsByClass("c1");
            for (Element commentElement : commentElements) {
                Element titleElement = commentElement.getElementsByClass("c2").first().getElementsByClass("c3").first();
                String name = titleElement.getElementsByTag("a").first().text();
                String date = titleElement.text().replace(name, "").replace("Posted on ", "").replace("by:", "").trim();
                Element contentElement = commentElement.getElementsByClass("c6").first();

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy,HH:mm zzz", Locale.ENGLISH);
                comments.add(new Comment(dateFormat.parse(date), name, contentElement.toString()));
            }
            //長さ
            Element lengthElement = document.getElementById("gdd").child(0).child(0).child(5).child(1);
            //タグ
            int length = Integer.valueOf(lengthElement.text().substring(0, lengthElement.text().indexOf("pages")).trim());
            Elements tagParentElements = document.getElementsByClass("tc");
            ArrayList<Tag>[] tagGroups = new ArrayList[TagType.values().length];
            for (int i = 0; i < tagGroups.length; i++) {
                tagGroups[i] = new ArrayList<>();
            }
            for (Element tagFamilyElement : tagParentElements) {
                TagType type = TagType.MISC;
                switch (tagFamilyElement.text()) {
                    case "parody:":
                        type = TagType.PARODY;
                        break;
                    case "character:":
                        type = TagType.CHARACTER;
                        break;
                    case "group:":
                        type = TagType.GROUP;
                        break;
                    case "artist:":
                        type = TagType.ARTIST;
                        break;
                    case "male:":
                        type = TagType.MALE;
                        break;
                    case "female:":
                        type = TagType.FEMALE;
                        break;
                    case "misc:":
                        type = TagType.MISC;
                        break;
                }
                ArrayList<Tag> tags = new ArrayList<>();
                for (Element tagElement : tagFamilyElement.parent().child(1).children()) {
                    String tagName = tagElement.child(0).attr("onclick");
                    tagName = tagName.substring(tagName.indexOf("tagmenu") + 9, tagName.indexOf("',"));
                    if (tagName.contains(":")) {
                        tagName = tagName.substring(tagName.indexOf(":") + 1);
                    }
                    tags.add(new Tag(tagName.trim(), type));
                }
                tagGroups[type.ordinal()] = tags;
            }
            Element rateElement = document.getElementById("rating_label");
            float rate = Float.valueOf(rateElement.text().substring(rateElement.text().indexOf("Average: ") + "Average: ".length()));
            return new Gallery(id,document.getElementById("gn").text(), document.getElementById("gj").text(), length, rate, mediaIds, tagGroups, comments);
        } catch (IOException | java.text.ParseException ex) {
            throw new ParseException(ex);
        }

    }

    @Override
    public Gallery parseGallery(GalleryId id) throws ParseException {
        try {
            Connection.Response response = Jsoup.connect(id.getUrl()).userAgent("Mozilla").cookies(config.getCookies()).execute();
            return privateParseGallery(id, response);
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    @Override
    public Gallery parseGallery(String url) throws ParseException {
        try {
            Connection.Response response = Jsoup.connect(url).userAgent("Mozilla").cookies(config.getCookies()).execute();
            Document document = response.parse();
            String galleryId = url.substring(url.indexOf("/g/") + 3).split("/")[0];
            String galleryToken = url.substring(url.indexOf("/g/") + 3).split("/")[1];
            String galleryTitle = document.getElementById("gn").text();
            Element rateElement = document.getElementById("grt2").child(0);

            String rateString = rateElement.attr("style");
            float integerOfRate = Float.valueOf(rateString.substring(rateString.indexOf("background-position:") + "background-position:".length(), rateString.indexOf("px"))) / 16;
            float decimalOfRate = (Float.valueOf(rateString.substring(rateString.indexOf("px ") + "px ".length(), rateString.lastIndexOf("px"))) + 1) / 40;
            float galleryRate = 5 + integerOfRate + decimalOfRate;

            String galleryThumbnailUrl = document.getElementsByClass("gdtl").get(0).child(0).child(0).attributes().get("src");
            String galleryGenreString = document.getElementById("gdc").child(0).child(0).attributes().get("alt");
            Genre galleryGenre = Genre.getGenreByName(galleryGenreString);
            int favoriteFolderId = -1;
            Element favoriteElement = document.getElementById("gdf").child(0);
            if (favoriteElement.children().size() > 0) {
                String f = favoriteElement.child(0).attributes().get("style");
                f = f.substring(f.indexOf("background-position"));
                int n = Integer.valueOf(f.substring(f.indexOf("px -") + 4, f.indexOf("px;")));
                favoriteFolderId = (n - 2) / 19;
            }

            GalleryId id = new GalleryId(url, galleryId, galleryToken, galleryTitle, galleryRate, galleryThumbnailUrl, galleryGenre, favoriteFolderId);
            return privateParseGallery(id, response);
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    private GalleryPage privateParseGalleryPage(GalleryId id, int page) throws ParseException {
        try {
            GalleryPage mediaIds = new GalleryPage(id, page);
            Document document;
            Connection.Response response = Jsoup.connect(id.getUrl() + "?p=" + page).userAgent("Mozilla").cookies(config.getCookies()).execute();
            document = response.parse();
            Elements gallareyElements = document.getElementsByClass("gdtl");
            int number = page * 20;
            for (Element element : gallareyElements) {
                Element e = element.child(0);
                mediaIds.add(new MediaId(
                        e.attributes().get("href"),
                        e.child(0).attributes().get("src"),
                        id,
                        ++number
                ));
            }
            return mediaIds;
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    @Override
    public GalleryPage parseGalleryPage(GalleryId id, int page) throws ParseException {
        return privateParseGalleryPage(id, page);
    }

    @Override
    public GalleryPage parseGalleryPage(Gallery gallery, int page) throws ParseException {
        GalleryPage galleryPage = privateParseGalleryPage(gallery.getGalleryId(), page);
        gallery.addGalleryPage(galleryPage);
        return galleryPage;
    }

    @Override
    public Media parseMedia(MediaId id) throws ParseException {
        try {
            Connection.Response response = Jsoup.connect(id.getMediaUrl()).userAgent("Mozilla").cookies(config.getCookies()).execute();
            System.out.println(response.url());
            Document document = response.parse();
            Element imageElement = document.getElementById("i3").child(0).child(0);
            Element reLoadElement = document.getElementById("i6").getElementById("loadfail");
            String reload = reLoadElement.attributes().get("onclick");
            return new Media(imageElement.toString().substring(imageElement.toString().indexOf("src") + 5, imageElement.toString().indexOf("\" style")), id, reload.substring(reload.indexOf("(") + 2, reload.indexOf(")") - 1));
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }

    @Override
    public Media reLoadMedia(Media media) throws ParseException {
        try {
            Connection.Response response = Jsoup.connect(media.getMediaId().getMediaUrl()).userAgent("Mozilla").cookies(config.getCookies()).data("nl", media.getReLoadParam()).execute();
            Document document = response.parse();
            Element imageElement = document.getElementById("i3").child(0).child(0);
            Element reLoadElement = document.getElementById("i6").getElementById("loadfail");
            String reload = reLoadElement.attributes().get("onclick");
            return new Media(imageElement.toString().substring(imageElement.toString().indexOf("src") + 5, imageElement.toString().indexOf("\" style")), media.getMediaId(), reload.substring(reload.indexOf("(") + 2, reload.indexOf(")") - 1),true);
        } catch (IOException ex) {
            throw new ParseException(ex);
        }
    }
}
