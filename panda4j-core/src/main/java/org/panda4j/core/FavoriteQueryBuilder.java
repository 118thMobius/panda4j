/*
 * Copyright 2016 Panda4J.org.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.panda4j.core;

/**
 * お気に入りを取得するための{@link org.panda4j.core.FavoriteQuery}を実体化します。
 *
 * @author mobius
 */
public class FavoriteQueryBuilder implements QueryBuilder{
    private int folder = -1;

    /**
     * 取得するお気に入りフォルダを指定します。
     * -1を与えることですべてのお気に入りを取得します。
     * 
     * @param folder フォルダのインデックス
     * @return 自オブジェクト
     */
    public FavoriteQueryBuilder setFolder(int folder) {
        this.folder = folder;
        return this;
    }
    
    /**
     * {@link #setFolder(int) }により設定されたフォルダを取得します。
     * デフォルト値は-1です。
     * 
     * @return フォルダのインデックス
     */
    public int getFolder() {
        return folder;
    }
    
    @Override
    public Query build() {
        return new FavoriteQuery(folder);
    }
}
