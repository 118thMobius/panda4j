/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panda4j.core;

import org.panda4j.core.parser.AuthenticatedParser;
import org.panda4j.core.parser.ParseException;

/**
 *
 * @author mobius
 */
public class AuthenticatedPandaImpl extends PandaImpl implements AuthenticatedPanda{
    
    public AuthenticatedPandaImpl(AuthenticatedParser parser, Configuration config) {
        super(parser, config);
    }
    
    @Override
    public void addToFavorite(GalleryId id, int folder, String note) throws PandaException {
        //XXX:AuthencticatedなParserしか入ってこないハズだが？？？
        if (parser instanceof AuthenticatedParser) {
            try {
                ((AuthenticatedParser) parser).addToFavorite(id, folder, note);
            } catch (ParseException ex) {
                throw new PandaException(ex);
            }
        }
        throw new PandaException(new IllegalStateException("お気に入りが利用できるのは認証済みの場合のみです。"));
    }
    
    @Override
    public void removeFromFavorite(GalleryId id) throws PandaException {
        if (parser instanceof AuthenticatedParser) {
            try {
                ((AuthenticatedParser) parser).removeFromFavorite(id);
            } catch (ParseException ex) {
                throw new PandaException(ex);
            }
        }
        throw new PandaException(new IllegalStateException("お気に入りが利用できるのは認証済みの場合のみです。"));
    }
    
    @Override
    public String[] loadFavoriteFoldersTitle() throws PandaException {
        if (parser instanceof AuthenticatedParser) {
            try {
                return ((AuthenticatedParser) parser).parseFavoriteFoldersTitle();
            } catch (ParseException ex) {
                throw new PandaException(ex);
            }
        }
        throw new PandaException(new IllegalStateException("お気に入りが利用できるのは認証済みの場合のみです。"));
    }
}
